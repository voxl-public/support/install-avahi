#!/bin/bash

source avahi-install/package-names.sh

version_number=`cat avahi-install/version.txt`
archive_name=avahi-install_$version_number.tar.gz

rm -f $archive_name

cd avahi-install
rm -f *.ipk
wget https://storage.googleapis.com/modalai_public/modal_packages/archive/$common_ipk
wget https://storage.googleapis.com/modalai_public/modal_packages/archive/$core_ipk
wget https://storage.googleapis.com/modalai_public/modal_packages/archive/$daemon_ipk
wget https://storage.googleapis.com/modalai_public/modal_packages/archive/$avahi_ipk
wget https://storage.googleapis.com/modalai_public/modal_packages/archive/$nss_mdns_ipk
cd ..

echo "Creating tar file for avahi installer version $version_number"
tar -czvf $archive_name avahi-install/
